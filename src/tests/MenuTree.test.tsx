import { render, fireEvent, screen } from "@testing-library/react";
import { DataNode } from "antd/lib/tree";

import MenuTree from "../components/MenuTree";

function renderMenuTree(data: DataNode[] = []) {
  const defaultData: DataNode[] = [
    {
      title: "t-0",
      key: "k-0",
      children: [
        {
          title: "t-0-0",
          key: "k-0-0",
          children: [{ title: "t-0-0-0", key: "k-0-0-0", children: [] }],
        },
        { title: "t-0-1", key: "k-0-1", children: [] },
      ],
    },
    {
      title: "t-1",
      key: "k-1",
      children: [
        {
          title: "t-1-0",
          key: "k-1-0",
          children: [],
        },
        {
          title: "t-1-1",
          key: "k-1-1",
          children: [{ title: "t-1-1-0", key: "k-1-1-0", children: [] }],
        },
      ],
    },
  ];

  return render(<MenuTree data={defaultData} />);
}

describe("<MenuTree />", () => {
  test("should display initialy only first level elements", async () => {
    const { findAllByTitle } = renderMenuTree();

    const count0 = await findAllByTitle(/^t-0/);
    const count1 = await findAllByTitle(/^t-1/);

    expect(count0).toHaveLength(1);
    expect(count1).toHaveLength(1);
  });

  test("should display all parent`s first level children when its clicked", async () => {
    const { container } = renderMenuTree();

    const parent0 = container.getElementsByClassName("ant-tree-switcher");

    fireEvent.click(parent0[0]);
    const children = await screen.findAllByTitle(/^t-0-/);
    expect(children).toHaveLength(2);
  });
});
