import React from "react";
import create from "zustand";
import data from "../data.json";
import produce from "immer";
import { DataNode } from "antd/lib/tree";
import { persist } from "zustand/middleware"


export const parseData: any = (data: Object) =>
  Object.values(data).map((e: JsonElement) => ({
    title: e.name,
    key: e.id,
    children: Object.values(e.children).length ? parseData(e.children) : [],
  }));

export interface JsonElement {
  id: string;
  name: string;
  children: Object;
  level: number;
}
export interface State {
  expandedKeys: React.Key[];
  checked: { checked: React.Key[]; halfChecked: React.Key[] } | React.Key[];
  treeData: DataNode[];
}

export interface Store {
  state: State;

    onExpand: (expandedKeysValue: React.Key[]) => void;
    onCheck: (
      checked: { checked: React.Key[]; halfChecked: React.Key[] } | React.Key[]
    ) => void;
  
}

export const useStore = create<Store>(
   persist(
    (set): Store => {
      const setState = (fn: ({ state }: { state: State }) => void) =>
        set(produce(fn));

      const initialState = {
        expandedKeys: [],
        checked: [],
        treeData: parseData(data),
      };

      return {
        state: {
          ...initialState,
        },

          onExpand: (expandedKeysValue) =>
            setState(({ state }) => {
              state.expandedKeys = expandedKeysValue;
            }),
          onCheck: (checkedKeysValue) =>
            setState(({ state }) => {
              state.checked = checkedKeysValue;
            }),

      };
    },
    {
        name: "hi-test-storage"
      }
   )
);
