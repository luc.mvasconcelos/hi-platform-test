import "./App.css";
import MenuTree from "./components/MenuTree";
import { useStore } from "./store";

function App() {
  const { treeData } = useStore((store) => store.state);
  return MenuTree({data:treeData});
}

export default App;
