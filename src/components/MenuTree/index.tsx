import React from "react";
import { Tree } from "antd";
import { useStore } from "../../store";
import { DataNode } from "antd/lib/tree";

const MenuTree: React.FC<{data:DataNode[]}> = (props) => {
  const { expandedKeys, checked } = useStore((store) => store.state);
  const { onCheck, onExpand } = useStore((store) => store);
  console.log(`checked`, checked)
  return (
    <Tree
      checkable
      treeData={props.data}
      expandedKeys={expandedKeys}
      checkedKeys={checked}
      onCheck={onCheck}
      onExpand={onExpand}
    />
  );
};

export default MenuTree;
